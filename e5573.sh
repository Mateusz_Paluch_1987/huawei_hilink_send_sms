#!/bin/sh
MODEM_IP="192.168.8.1"
LOGIN='admin'
PASSWORD='admin'

phone=$1
message=$2

refresh_token()
{
    RESPONSE=`curl -s -X GET http://$MODEM_IP/api/webserver/SesTokInfo -H "__RequestVerificationToken: $TOKEN"  -H "Cookie: $COOKIE"`
    TOKEN=`echo "$RESPONSE"| grep TokInfo | cut -d '>' -f2 | cut -d '<' -f1`
    COOKIE=`echo "$RESPONSE"| grep SessionID= | cut -d '>' -f2 | cut -d '<' -f1`
}

login()
{
    PSD=$(echo -n $(echo -n $PASSWORD | sha256sum | sed 's/  -//') |  base64 -w 0)
    PSD=$(echo -n $(echo -n $LOGIN$PSD$TOKEN | sha256sum | sed 's/  -//') | base64 -w 0)
    DATA="<request><Username>$LOGIN</Username><Password>$PSD</Password><password_type>4</password_type></request>"
    RESPONSE=`curl -X POST -d $DATA "http://$MODEM_IP/api/user/login" -H "__RequestVerificationToken: $TOKEN"  -H "Cookie: $COOKIE" -D - -s`
    TOKEN=`echo "$RESPONSE" | grep "__RequestVerificationToken" |  cut -d ':' -f2 |  cut -d '#' -f5`
    COOKIE=`echo "$RESPONSE" | grep "Set-Cookie:" |  cut -d ':' -f2 | cut -d ';'  -f1`
}

send_sms()
{
TIME=$(date +"%Y-%m-%d %T")
SMS="<request><Index>-1</Index><Phones><Phone>$1</Phone></Phones><Sca></Sca><Content>$2</Content><Length>${#2}</Length><Reserved>1</Reserved><Date>$TIME</Date></request>"
curl -X POST -d "$SMS" "http://$MODEM_IP/api/sms/send-sms" -H "__RequestVerificationToken: $TOKEN" -H "Cookie: $COOKIE" -s > /dev/null
}


refresh_token
login
send_sms "$phone" "$message"
